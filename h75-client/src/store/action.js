import axios from 'axios';
const URL = 'http://localhost:8000';

export const ENCODE_TEXT = 'ENCODE_TEXT';
export const DECODE_TEXT = 'DECODE_TEXT';

export const encodeText = (text) => {
  return {type: ENCODE_TEXT, text}
};

export const encodeMessage = message => {
  return dispatch => {
    return axios.post(`${URL}/encode`, message).then(
      response => dispatch(encodeText(response.data))
    );
  };
};

export const decodeText = (text) => {
  return {type: DECODE_TEXT, text}
};

export const decodeMessage = message => {
  return dispatch => {
    return axios.post(`${URL}/decode`, message).then(
      response => dispatch(decodeText(response.data))
    );
  };
};

