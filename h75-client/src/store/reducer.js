import {DECODE_TEXT, ENCODE_TEXT} from "./action";

const initialState = {
  decodedText: '',
  encodedText: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ENCODE_TEXT:
      return {...state, encodedText: action.text.encoded};
    case DECODE_TEXT:
      return {...state, decodedText: action.text.decoded};
    default:
      return state;
  }
};

export default reducer;